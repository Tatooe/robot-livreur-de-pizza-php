<?php
class RobotLivreurPizza
{
    //Attributs
    private $MessageEcran = "";
    private const CAPA_AFFICH = 255;
    private $NomDevWeb = "Tatiana";

    
    

    /**
     * Instance ! RobotLivreurPizza _ Constructeur
     * Pas de constructeur car chaque nouveaux robors pourra avoir des méthodes dictées aléatoirement.
     */
public function __construct(string $NomDevWeb){
    $this-> SetName($NomDevWeb);
    }


//Methodes publics


    /**
     * Methode! function Avancer()
     * Méthode pour faire avancer le robot
     * @param float $Distance Pour le méthodes Avancer() le chiffre devra être supérieur à 0,
     *                        Pour la méthode Reculer() le chiffre devra être inférieur à 0,
     * 
     * @param $Obstacle valeur booléenne true pour présence d'un obstacle
     * 
        */
    public function AvancerReculer(float $Distance, bool $Obstacle)
    {
        if ($Obstacle == false) {
            if (is_float($Distance) && 0 <= $Distance) {
                $this->AfficherMessage(printf('J\'avance de %s mètres.</br>', $Distance));
            } else if (is_float($Distance) && 0 >= $Distance) {
                $this->AfficherMessage(printf('Je recule de %s mètres.</br>', $Distance * -1));
            } else {
                $Distance = 0;
                printf('Merci de renseigner une valeur supérieur à 0</br>');
            }
        } else {
            printf('BipBipBip!!!  Désolé %s je suis coincé</br>', $this->NomDevWeb);
        }
    }

    /**
     * Methode! function TournerDroite()
     * Méthode pour faire tourner à droite le robot
     * @param int $Rotation le chiffre doit être compris entre 0 et 180 (positif pour TournerDroite, négatif pour TournerGauche) 
     * celui-ci correspond à l'angle de rotation du robot
     * 
     * @param $Obstacle valeur booléenne true pour présence d'un obstacle
     * 
     */
    public function Tourner(float $Rotation, bool $Obstacle)
    {
        if (is_bool($Obstacle) && $Obstacle == false) {
            if (is_float($Rotation) && 0 <= $Rotation && 180 >= $Rotation) {
                $this->Rotation = $Rotation;
                $this->AfficherMessage(printf("Je tourne à droite de %s  °.</br>", $Rotation));
            } else if (is_float($Rotation) && 0 >= $Rotation && -180 <= $Rotation) {
                $this->Rotation = $Rotation;
                $this->AfficherMessage(printf("Je tourne à gauche de %s  °.</br>", $Rotation * -1));
            } else {
                $Rotation = 0;
                $this->AfficherMessage(printf("Mauvais angle de rotation renseigné</br>"));
            }
        } else {
            printf("BipBipBip!!!Désolé %s je suis coincé !!!</br>", $this->NomDevWeb);
        }
    }

    /**
    * Méthode DebloquerRobot()
    * @param $Debloquer la valeur booléen, true décoincé, false encore bloqué
    *  
    */
public function DebloquerRobot(bool $Debloquer) {
    if(is_bool($Debloquer) && true == $Debloquer) {
        $this->AfficherMessage(printf("Merci %s, tu peux retourner travailler </br>", $this->NomDevWeb));
    }else{
        $this->AfficherMessage(printf("BipBipBip!!!! C'est pas possible ce chat !!grrr</br>"));
    }
}


    /**
     * Methode! function MonterPlateau()
     * Méthode pour faire monter le plateau
     * 
     * @param int $MovePlateau Le chiffre doit être compris entre 0 et 50 cm pour monter et -50 à 0 pour descendre,
     * info: le plateau est à 40 cm du sol et ne peut pas monter au dessus de 85cm
     * 
     */
    public function MouvementPlateauZ(float $MovePlateauZ){
        if ( is_float($MovePlateauZ) && 0 <= $MovePlateauZ && 50 >= $MovePlateauZ){
            $this->MovePlateauZ = $MovePlateauZ;
            $this->AfficherMessage(printf('Attention le plateau monte de %s cm </br>', $MovePlateauZ));
        } else if (is_float($MovePlateauZ) && 0 >= $MovePlateauZ && -50 <= $MovePlateauZ) {
            $this->MovePlateauZ = $MovePlateauZ;
            $this->AfficherMessage(printf("Attention le plateau descend de %s cm </br>", $MovePlateauZ * -1));           
        } else {
            $this->AfficherMessage(printf('Les valeurs sont hors limites</br>'));
        }
    }

    


    /**
     * Methode! function MouvementPizza()
     * Méthode pour pousser le plateau de pizza à l'arriver de la livraison de pizza
     * 
     * @param int $MovePlateauX valeur entre 0 et 30 
     * positif, pousse le plateau,négative tire la pizza du four
     * 
     */
    public function MouvementPizza(float $MovePlateauX)
    {
        if (is_float($MovePlateauX) && 0 <= $MovePlateauX && 30 >= $MovePlateauX) {
            $this->MovePlateauX = $MovePlateauX;
            $this->AfficherMessage(printf('Pizza Poussée OK</br>'));
            $this->AfficherMessage(printf('Bon Appetit %s !</br>', $this->NomDevWeb));
        } else if (is_float($MovePlateauX) && 0 >= $MovePlateauX && -30 <= $MovePlateauX) {
            $this->MovePlateauX = $MovePlateauX; 
            $this->AfficherMessage(printf('Pizza sur Plateau OK</br>'));
        }else{
            $this->AfficherMessage(printf('Les valeurs sont hors limites</br>')); 
        }
    }


        /**
     * Methode! function SetName()
     * Afin de personalisé le nom du mangeur de pizza
     *  
     * @param int $NouveauNom Valeur attendu String
     * 
     */
    public function SetName(string $NomDevWeb)
    {
        if(is_string($NomDevWeb)){
        $this->NomDevWeb = $NomDevWeb;}
    }

//Méthodes Privées

    /**
    * Méthode pour afficher Message
    * @param $Message la valeur doit être une string
    *  
    */
    private function AfficherMessage(string $Message)
    {
        $this->MessageEcran = $this->TesterLongueurMessage($Message);
        print($this->MessageEcran);
    }




    /**
     * Vérifie que le message affiché ne dépasse pas la capacité de l'écran
     * 
     * @param string $MessagePossible Valeur du message avant réduction de la longueur
     * @return string Valeur du message après réduction éventuelle de la longueur
     */

    private function TesterLongueurMessage(string $MessagePossible)
    {
        if (self::CAPA_AFFICH < strlen($MessagePossible)) {
            $MessagePossible = substr_replace($MessagePossible, "", 255);
        }        
    }


    }


//Programme
$RobotPizzaFromage = new RobotLivreurPizza("Tatiana");
$RobotPizzaFromage -> AvancerReculer(5,false);
$RobotPizzaFromage ->Tourner(-90, false);
$RobotPizzaFromage -> AvancerReculer(2, false);
$RobotPizzaFromage ->Tourner(-90, true);
$RobotPizzaFromage ->DebloquerRobot(false);
$RobotPizzaFromage ->DebloquerRobot(true);
$RobotPizzaFromage ->Tourner(-90,false);
$RobotPizzaFromage ->AvancerReculer(2,false);
$RobotPizzaFromage ->MouvementPlateauZ(45);
$RobotPizzaFromage ->MouvementPizza(-25);
$RobotPizzaFromage ->MouvementPlateauZ(-45);
$RobotPizzaFromage ->AvancerReculer(-2, false);
$RobotPizzaFromage ->Tourner(90,false);
$RobotPizzaFromage -> AvancerReculer(5, false);
$RobotPizzaFromage ->Tourner(90,false);
$RobotPizzaFromage -> AvancerReculer(5, false);
$RobotPizzaFromage ->Tourner(90,false);
$RobotPizzaFromage -> AvancerReculer(10, false);
$RobotPizzaFromage ->Tourner(-90,false);
$RobotPizzaFromage -> AvancerReculer(1, false);
$RobotPizzaFromage ->Tourner(-90,false);
$RobotPizzaFromage -> AvancerReculer(1, false);
$RobotPizzaFromage ->MouvementPlateauZ(45);
$RobotPizzaFromage ->MouvementPizza(25);





